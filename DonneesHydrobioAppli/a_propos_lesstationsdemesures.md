# Les stations de mesures

![](www/CarteReseau.png)

Les stations de prélèvements biologiques appartiennent au programme de surveillance de la Directive Cadre sur l'Eau (DCE) à travers différents réseaux de suivi.

Trois réseaux principaux :

-   Le **Réseau de Référence Pérenne** (RRP) pour évaluer les conditions de référence qui servent à définir le bon état écologique de la DCE par type de masse d'eau.

-   Le **Réseau de Contrôle de Surveillance** (RCS) pour évaluer l'état général des eaux et à suivre les changements à long terme de l'état des eaux suite à des changements d'origines naturelle ou anthropique.

-   Le **Réseau de Contrôles Opérationnels** (RCO) pour masses d'eau identifiées comme risquant de ne pas atteindre les objectifs environnementaux de la DCE.

Deux réseaux complémantaires :

-   Le **Réseau Complémentaire Agence** (RCA) pour acquérir des données.

-   Le **Reseau Départemental** (RD) en soutiens des différents réseau ci-dessus.

Sur la période 2007 à 2019, en Pays de la Loire, 603 stations sur lesquelles des prélèvements biologiques sont effectué et recensés dans cette application.
