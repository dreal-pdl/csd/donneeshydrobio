
###### Packages ######
library(shiny)
library(shinydashboard)
library(ggplot2)
library(plotly)
library(DT)
library(reactable)
library(tidyverse)
library(leaflet)
library(bsplus)
library(shinyBS)

load(file = "Fichiers.RData")
